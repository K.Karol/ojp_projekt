#include "utils.hpp"

// STRUCT Text_no_spaces

// Constructor
Text_no_spaces::Text_no_spaces(const string& text){
  for (unsigned int i = 0; i < text.length(); i++) {
    if (isspace(text[i])) {
      space_positions.push_back(i);
    } else {
      text_no_spaces += text[i];
    }
  }
};
// Assignment operator
Text_no_spaces& Text_no_spaces::operator=(const string& text){
  Text_no_spaces temp(text);
  text_no_spaces = temp.get_text();
  space_positions = temp.get_space_positions();
  return *this;
};

// Functions

bool is_hexadecimal(const string& hex_str){
  regex pattern {R"(^(?:0[xX])?[0-9a-fA-F ]*$)"};
  smatch matches;
  // Split text (regex search does not work correctly with 5000char+ strings?)
  unsigned int safe_length = 2000;
  vector<string> hex_strings;
  for (unsigned int i = 0; i < hex_str.length(); i += safe_length) {
    hex_strings.push_back(hex_str.substr(i, safe_length));
  }
  // Regex search
  for (string hex_s : hex_strings) {
    if (!regex_search(hex_s, matches, pattern)) {
      return false;
    }
  }
  return true;
}

bool is_starting_with_hex_prefix(const string& text){
  string str = text.substr(0, 2);
  return (str == "0x" ||str == "0X");
}

string complete_byte_string(const string& start_text){
	string temporary = start_text;
  if (start_text.length() % 2) {  // add 0 on the end if odd
    temporary += '0';
  }
	return temporary;
}

string get_correct_text(const string& text){
  string final_text = "";
  stringstream ss(text);
  for (string word; ss >> word; ) {
    if (word.length() % 2) {
      word += '0';
    }
    final_text += word + ' ';
  }
  final_text.pop_back();  // remove last space
  return final_text;
}

vector<char> str_to_vec(const string& str){
  stringstream ss(str);
  vector<char> vec;
  for (char c; ss >> c; ) {
    vec.push_back(c);
  }
  return vec;
}

vector<int> bin_str_to_vec(const string& str){
  vector<int> vec;
  for (char c : str) {
    vec.push_back(c == '0' ? 0 : 1);
  }
  return vec;
}

string vec_to_str(const vector<int>& vec){
	stringstream ss;
	string result;
	for (unsigned int i = 0; i < vec.size(); i++)	{
		ss << vec[i];
	}
	ss >> result;
	return result;
}

string bin_to_hex(const vector<int>& bin_vec){
  string bin_str = vec_to_str(bin_vec);
	string hex_str;
	stringstream ss;
	bitset<bitset_size> bset(bin_str);
	ss << setfill('0') << setw(2) << hex << bset.to_ulong();
	ss >> hex_str;
	return hex_str;
}

string hex_to_bin(const string& hex_str){
	unsigned int val;
	string bin_str;
	stringstream ss;
	ss << hex << hex_str;
	ss >> val;
	bitset<bitset_size> bset(val);
	bin_str = bset.to_string();
	return bin_str;
}

vector<int> dec_to_binary(int dec, unsigned int precision){  // default precision = 2
  string bin_str;
  int temp = dec;
  while (temp != 0) {
    bin_str = (temp % 2 == 0 ? '0' : '1') + bin_str;
    temp /= 2;
  }
  // Make sure that bin value is correct length (if given - default 2)
  if (precision > bin_str.size()) {
    bin_str.insert(0, precision - bin_str.size(), '0');
  }
  // Convert string to vector and return it
  vector<int> bin_vec = bin_str_to_vec(bin_str);
	return bin_vec;
}

int bin_to_dec(const vector<int>& bin_vec){
	string bin_str = vec_to_str(bin_vec);
	int dec = stoi(bin_str, nullptr, 2);
	return dec;
}

vector<int> xor_vec(const vector<int>& base, const vector<int>& to_add){
	vector<int> sum;
	if (base.size() != to_add.size()) {
		error("addition parts sizes not equal");
	}

	for (unsigned int i = 0; i < base.size(); i++) {
		if (base[i] + to_add[i] == 2 || base[i] + to_add[i] == 0) {
			sum.push_back(0);
		} else if (base[i] + to_add[i] == 1) {
			sum.push_back(1);
		}	else {
			error("wrong (non binary) digits in text");
		}
	}
	return sum;
}

vector<int> fuse_vect(const vector<int>& first, const vector<int>& second){
	vector<int> fused;
	if (first.size() != second.size()) {
		error("vectors passed to fuse are not equal");
	}

	for (unsigned int i = 0; i < first.size(); i++) {
		fused.push_back(first[i]);
	}
	for (unsigned int i = 0; i < second.size(); i++) {
		fused.push_back(second[i]);
	}
	return fused;
}

void split_in_half(const vector<int>& base, vector<int>& half_f, vector<int>& half_s){
	half_f.clear();
	half_s.clear();

	for (unsigned int i = 0; i < base.size(); i++) {
		if (i < (base.size() / 2)) {    // put first half digits in first vector
			half_f.push_back(base[i]);
		} else {
			half_s.push_back(base[i]);    // put second half digits in second vector
		}
	}
	if (half_f.size() != half_s.size()) {
		error("unequal halves of vector (base vector must be even!)");
	}
}

void save_text_to_file(const string& text){
  string file_name;
  cout << "\nPlease enter file name: ";
  cin >> file_name;

  ofstream ost {file_name};
  if (!ost) {
    error("can't open output file ", file_name);
  }

  ost << text;
}

string read_from_file(const string& file_name){
  string text = "";
  string line;
  ifstream ist {file_name};
  ist.exceptions(ist.exceptions()|ios_base::badbit); // make ist throw if it goes bad
  if (!ist) {
    error("can't open input file ", file_name);
  }

  while (getline(ist, line)) {
    text += line + ' ';
  }
  text.pop_back();  // remove last space
  return text;
}

string get_hex_text_from_file(){
  string file_name;
  string text;
  bool is_hex;
  do {
    cout << "Please enter file name: ";
    cin >> file_name;

    text = read_from_file(file_name);

    is_hex = is_hexadecimal(text);
    if (!is_hex) {
      system("CLS");
      cout << "Hint: only hexadecimal characters are allowed. Try with other input data...\n";
    }
  } while (!is_hex);
  system("CLS");
  return text;
}

string get_hex_text_from_cmd(){
  string text;
  bool is_hex;
  do {
    cout << "Please enter text in hexadecimal: ";
    getline(cin, text);

    is_hex = is_hexadecimal(text);
    if (!is_hex) {
      system("CLS");
      cout << "Hint: only hexadecimal characters are allowed.\n";
    }
  } while (!is_hex);
  system("CLS");
  return text;
}

