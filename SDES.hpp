#ifndef SDES_hpp
#define SDES_hpp

#include "utils.hpp"
#include "matrix.hpp"
#include "matrix.cpp"

class SDES {
public:
  SDES(){ };
  vector<int> sbox(const vector<int>& input, const SBox_Matrix<char, 4>& pattern);
  vector<int> permutation(const vector<int>& vec, const vector<int>& base);

  void set_key_from_cmd();

  vector<int> create_key(
    vector<int>& fkey_half,
    vector<int>& skey_half,
    vector<int>& key,
    const vector<int>& sl_perm
  );
  vector<vector<int>> get_round_keys(bool should_print);

  vector<int> round_encrypt(const vector<int>& round_key, const vector<int>& text);
  vector<int> encrypt(
    const vector<int>& text_part,
    const vector<int>& first_round_key,
    const vector<int>& second_round_key,
    bool should_print
  );
private:
  // Permutations
  vector<int> p10 = { 2, 4, 1, 6, 3, 9, 0, 8, 7, 5 };
  vector<int> p4w8 = { 3, 0, 1, 2, 1, 2, 3, 0 };
  vector<int> p10w8 = { 5, 2, 6, 3, 7, 4, 9, 8 };
  vector<int> p4 = { 1, 3, 2, 0 };
  vector<int> pw = { 1, 5, 2, 0, 3, 7, 4, 6 };
  vector<int> po = { 3, 0, 2, 4, 6, 1, 7, 5 };
  vector<int> sl1 = { 1, 2, 3, 4, 0 };
  vector<int> sl2 = { 2, 3, 4, 0, 1 };
  vector<int> cross = { 4, 5, 6, 7, 0, 1, 2, 3 };
  // SBoxes
  matrix_hpp::SBox_Matrix<char, 4> sbox1{ "1032321002133132" };
  matrix_hpp::SBox_Matrix<char, 4> sbox2{ "0123201330102103" };
  // User data
  vector<int> start_text;
  vector<int> start_key;
};

#endif
