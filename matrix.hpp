#ifndef matrix_hpp
#define matrix_hpp

template<typename T> class Matrix {
public:
  Matrix(){ };
  Matrix(const vector<T>& vec);

	T get_element(int row, int column) const;
	void print() const;

	void set_element(T var, int row, int column);
	void push_row(const vector<T>& var);
private:
	vector<vector<T>> m;
};

template<typename T, int n> class SBox_Matrix : public Matrix<T> {
public:
  SBox_Matrix(const string& str);
};

#endif
