#include "matrix.hpp"

// MATRIX

// Constructor
template<typename T> Matrix<T>::Matrix(const vector<T>& vec){
  for (const T val : vec) {
    m.push_back(val);
  }
};
// Getter
template<typename T> T Matrix<T>::get_element(int row, int column) const{
  return m[row][column];
};
// Print
template<typename T> void Matrix<T>::print() const{
  for (unsigned int i = 0; i < m.size(); i++) {
    cout << "| ";
    for (int j = 0; j < m.size(); j++) {
      string spacer = (j != m.size() - 1) ? ", " : " ";
      cout << m[i][j] << spacer;
    }
    cout << "|\n";
  }
};
// Setters
template<typename T> void Matrix<T>::set_element(T var, int row, int column){
  vector<int> position = {row, column};
  int index = bin_to_dec(position);   // 0 = 00, 1 = 01, 2 = 10, 3 = 11
  m[index] = var;
};

template<typename T> void Matrix<T>::push_row(const vector<T>& var){
  m.push_back(var);
};

// SBOX MATRIX

template<typename T, int n> SBox_Matrix<T, n>::SBox_Matrix(const string& str){
  for (unsigned int i = 0; i < str.length(); i += n) {
    string part = str.substr(i, n);
    vector<T> vec = str_to_vec(part);
    Matrix<T>::push_row(vec);
  }
};
