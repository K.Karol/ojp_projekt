#ifndef utils_hpp
#define utils_hpp

#include "std_lib_facilities.hpp"

constexpr int bitset_size = 8;

// STRUCT Text_no_spaces

struct Text_no_spaces {
public:
  // Constructors
  Text_no_spaces(){ };
  Text_no_spaces(const string& text);
  // Getters
  string get_text(){
    return text_no_spaces;
  };

  vector<int> get_space_positions(){
    return space_positions;
  };
  // Assignment operator
  Text_no_spaces& operator=(const string& text);
private:
  string text_no_spaces = "";
  vector<int> space_positions;
};

// Functions

bool is_hexadecimal(const string& hex_str);
bool is_starting_with_hex_prefix(const string& text);
string complete_byte_string(const string& start_text);
string get_correct_text(const string& text);

vector<char> str_to_vec(const string& str);
vector<int> bin_str_to_vec(const string& str);
string vec_to_str(const vector<int>& vec);
string bin_to_hex(const vector<int>& bin_vec);
string hex_to_bin(const string& hex_str);
vector<int> dec_to_binary(int dec, unsigned int precision = 2);
int bin_to_dec(const vector<int>& bin_vec);

vector<int> xor_vec(const vector<int>& add_base, const vector<int>& to_add);
vector<int> fuse_vect(const vector<int>& first, const vector<int>& second);
void split_in_half(const vector<int>& base, vector<int>& half_f, vector<int>& half_s);

void save_text_to_file(const string& text);
string read_from_file(const string& file_name);
string get_hex_text_from_file();
string get_hex_text_from_cmd();

template<typename T> void print_vec(const vector<T>& vec){  // must be in the hpp file (template must be visible to translation unit)
	cout << "{ ";
	for (auto val : vec) {
		cout << val << " ";
	}
	cout << "}";
}

#endif
