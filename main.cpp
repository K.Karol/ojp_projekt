/* Multithread S-DES, Karol Kaplanek 174085 */
#include "utils.hpp"
#include "SDES.hpp"
#include <conio.h>
#include <future>
#include <ctime>
// #include <unistd.h> usleep(1000);

int main(){
  SDES sdes;
  Text_no_spaces text_no_spaces;
	string text_start;
	string text_start_no_spaces;
	string text_encrypted;
	vector<int> space_indexes;
	vector<future<vector<int>>> futures;
	bool has_hex_prefix;
	bool should_print_steps;
	bool should_decrypt;

	try {
	  // Menu
	  cout << "Hello! Press:\n"
         << " 1 to enter text via CMD\n"
         << " 2 to enter text via entering file name\n"
         << " 3 to decrypt text (via CMD)\n"
         << " 4 to decrypt text (via text file)\n"
         << " Anything else to quit...\n\n";
	  switch (getch()) {
    case '1':
      text_start = utils_hpp::get_hex_text_from_cmd();
      should_decrypt = false;
      break;
    case '2':
      text_start = get_hex_text_from_file();
      should_decrypt = false;
      break;
    case '3':
      text_start = get_hex_text_from_cmd();
      should_decrypt = true;
      break;
    case '4':
      text_start = get_hex_text_from_file();
      should_decrypt = true;
      break;
    default:
      return 0;
	  }
	  cout << "Also... do you want to see the steps? (y / n): ";
	  switch (getch()) {
    case 'y':
      should_print_steps = true;
      break;
    case 'n':
    default:
      should_print_steps = false;
	  }
	  system("CLS");
	  // Make sure that all 'words' are even
	  text_start = get_correct_text(text_start);
	  // Fill all 'no_space' helpers
	  text_no_spaces = text_start;
	  text_start_no_spaces = text_no_spaces.get_text();
	  has_hex_prefix = is_starting_with_hex_prefix(text_start_no_spaces);
	  space_indexes = text_no_spaces.get_space_positions();
	  // Get start key from user
	  sdes.set_key_from_cmd();
	  // Get round keys
	  vector<vector<int>> keys = sdes.get_round_keys(true);
	  if (should_decrypt) {  // decryption (swap first and second round keys)
      swap(keys[0], keys[1]);
	  }
// clock_t time_start = clock();
    // Encryption loop
    unsigned int i = has_hex_prefix ? 2 : 0;   // start from the correct byte
    for (; i < text_start_no_spaces.length(); i += 2) {
      string text_part = complete_byte_string(text_start_no_spaces.substr(i, 2));
      string text_part_bin = hex_to_bin(text_part);
      vector<int> text_part_bin_vec = bin_str_to_vec(text_part_bin);
      auto fut = async(
        launch::async,
        &SDES::encrypt, &sdes,
        text_part_bin_vec,
        keys[0],
        keys[1],
        false
      );
      futures.push_back( move(fut) );
    }
    // Final result
    text_encrypted = has_hex_prefix ? text_start_no_spaces.substr(0, 2) : "";
    i = has_hex_prefix ? 2 : 0;   // start from the correct byte
    for (auto& fut : futures) {
      vector<int> vec = fut.get();
      string hex_value = bin_to_hex(vec);
      if (should_print_steps) {
        string hex_value_start = text_start_no_spaces.substr(i, 2);
        vector<int> vec_start = bin_str_to_vec(hex_to_bin(hex_value_start));
        // Print step encryption
        print_vec<int>(vec_start);
        cout << "(" << setw(2) << hex_value_start << ") -> ";
        print_vec<int>(vec);
        cout << "(" << setw(2) << hex_value << ")\n";
      }
      text_encrypted += hex_value;
      i += 2;
    }
// clock_t time_end = clock();
    // Add spaces to the encrypted string
    for (int space_index : space_indexes) {
      text_encrypted.insert(space_index, " ");
    }
    // Print the simplified result
    cout << "\nStart text:\n" << text_start
         << "\n\nEncrypted:\n" << text_encrypted <<endl;
// << "\n\nTime spent: " <<  double(time_end - time_start) / CLOCKS_PER_SEC <<endl;
	  cout << "\nDo you want to save the output to the file? (y / n): ";
	  switch (getch()) {
    case 'y':
      save_text_to_file(text_encrypted);
      break;
    case 'n':
    default:
      ; // pass
    }

    return 0;
  } catch (exception& e) {
    cerr << "Error: " << e.what() <<endl;
    return 1;
  } catch (...) {
    cerr << "Oopsie: unknown exception!\n";
    return 2;
  }
}
