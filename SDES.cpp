#include "SDES.hpp"

vector<int> SDES::sbox(const vector<int>& input, const SBox_Matrix<char, 4>& pattern){
  vector<int> output;
  vector<int> row_vec;
  vector<int> column_vec;
  int row = 0;      // row number is a decimal made from binary of first and last element of a vector
  int column = 0;   // column number is a decimal made from binary of middle part of vector
  int output_dec;
  // Calculate row
  row_vec.push_back(input[0]);
  row_vec.push_back(input[(input.size() - 1)]);
  row = bin_to_dec(row_vec); // turns row_vec into decimal number
  // Calculate column
  for (unsigned int i = 1; i < input.size() - 1; i++) {
    column_vec.push_back(input[i]);
  }
  column = bin_to_dec(column_vec);
  // Get proper SBox value and return it
  output_dec = int(pattern.get_element(row, column)) - int('0'); // ASCII - number (char) value - first number (char) value
  output = dec_to_binary(output_dec);
  return output;
};

vector<int> SDES::permutation(const vector<int>& vec, const vector<int>& base){
  vector<int> changed;
  for (unsigned int i = 0; i < base.size(); i++) {
    if (base[i] >= vec.size()) {  // just to make sure
      error("permutation base calling non-existent vector element");
    }
    changed.push_back(vec[base[i]]);
  }
  return changed;
};

void SDES::set_key_from_cmd(){
  vector<int> key;
  string key_start_str;
  regex bit_pattern {R"(^([{(])?[01, ]*([)}])?$)"};
  smatch matches;
  do {
    cout << "Please enter 10-bit key (write '}', ')' or ';' to end): ";
    for (char temp; cin >> temp;) {
      if (temp == '0' || temp == '1') {
        key.push_back(
          temp == '0'
            ? 0
            : temp == '1'
              ? 1
              : temp
        );
      } else if (key.size() != 10) {
        system("CLS");
        cout << "Hint: key must be 10-bit long.\n";
        key.clear();
      } else if (temp == '}' || temp == ')' || temp == ';') {
        break;
      } else if (temp != '{' && temp != '(' && temp != ' ' && temp != ',') {
        system("CLS");
        cout << "Hint: available notations: {0, 1, 0, 1}, 0101, 0 1 0 1, (0,1,0,1) etc.\n";
        key.clear();
      }
    }
    key_start_str = vec_to_str(key);
  } while (!regex_search(key_start_str, matches, bit_pattern));
  // Set start_key attribute
  start_key = key;
  // Clear console
  system("CLS");
};

vector<int> SDES::create_key(
  vector<int>& fkey_half,
  vector<int>& skey_half,
  vector<int>& key,
  const vector<int>& sl_perm
){
  vector<int> round_key;

  split_in_half(key, fkey_half, skey_half);
  fkey_half = permutation(fkey_half, sl_perm);
  skey_half = permutation(skey_half, sl_perm);

  key = fuse_vect(fkey_half, skey_half);
  round_key = permutation(key, p10w8);
  return round_key;
};

vector<vector<int>> SDES::get_round_keys(bool should_print){
  vector<int> key_permutation;
  vector<int> fkey_half, skey_half;
  vector<int> first_round_key;
  vector<int> second_round_key;
  vector<vector<int>> keys;
  // First round key creation
  key_permutation = permutation(start_key, p10);
  first_round_key = create_key(fkey_half, skey_half, key_permutation, sl1);
  if (should_print) {
    cout << "First round key:\t";
    print_vec(first_round_key);
    cout <<endl;
  }
  // Second round key creation
  second_round_key = create_key(fkey_half, skey_half, key_permutation, sl2);
  if (should_print) {
    cout << "Second round key:\t";
    print_vec(second_round_key);
    cout <<endl;
  }
  // Return round keys
  keys = { first_round_key, second_round_key };
  return keys;
};

vector<int> SDES::round_encrypt(const vector<int>& round_key, const vector<int>& text){
  vector<int> ftext_half, stext_half;
  vector<int> temp_first, temp_second;
  vector<int> temp, output;

  split_in_half(text, ftext_half, stext_half);
  temp = stext_half;
  temp = permutation(temp, p4w8);
  temp = xor_vec(temp, round_key);

  split_in_half(temp, temp_first, temp_second);
  temp_first = sbox(temp_first, sbox1);
  temp_second = sbox(temp_second, sbox2);

  temp = fuse_vect(temp_first, temp_second);
  temp = permutation(temp, p4);
  temp = xor_vec(ftext_half, temp);

  output = fuse_vect(temp, stext_half);
  return output;
};

vector<int> SDES::encrypt(
  const vector<int>& text_part,
  const vector<int>& first_round_key,
  const vector<int>& second_round_key,
  bool should_print
){
  vector<int> temp, output;
  // Introducing permutation
  temp = permutation(text_part, pw);
  // Starting first round
  temp = round_encrypt(first_round_key, temp);
  if (should_print) {
    cout << "After first round: " << bin_to_hex(temp) << "\n";
  }
  // Crossing
  temp = permutation(temp, cross);
  // Starting second round
  temp = round_encrypt(second_round_key, temp);
  if (should_print) {
    cout << "After second round: " << bin_to_hex(temp) << "\n";
  }
  // Reversed permutation
  output = permutation(temp, po);
  if (should_print) {
    cout << "Encrypted fragment: ";
    print_vec(output);
    cout <<endl;
  }
  // Return encrypted text
  return output;
};
